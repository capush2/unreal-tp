// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOFTDESIGNTRAINING_SoftDesignTrainingPlayerController_generated_h
#error "SoftDesignTrainingPlayerController.generated.h already included, missing '#pragma once' in SoftDesignTrainingPlayerController.h"
#endif
#define SOFTDESIGNTRAINING_SoftDesignTrainingPlayerController_generated_h

#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_SPARSE_DATA
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_RPC_WRAPPERS
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASoftDesignTrainingPlayerController(); \
	friend struct Z_Construct_UClass_ASoftDesignTrainingPlayerController_Statics; \
public: \
	DECLARE_CLASS(ASoftDesignTrainingPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), NO_API) \
	DECLARE_SERIALIZER(ASoftDesignTrainingPlayerController)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_INCLASS \
private: \
	static void StaticRegisterNativesASoftDesignTrainingPlayerController(); \
	friend struct Z_Construct_UClass_ASoftDesignTrainingPlayerController_Statics; \
public: \
	DECLARE_CLASS(ASoftDesignTrainingPlayerController, APlayerController, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), NO_API) \
	DECLARE_SERIALIZER(ASoftDesignTrainingPlayerController)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASoftDesignTrainingPlayerController(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASoftDesignTrainingPlayerController) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASoftDesignTrainingPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoftDesignTrainingPlayerController); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASoftDesignTrainingPlayerController(ASoftDesignTrainingPlayerController&&); \
	NO_API ASoftDesignTrainingPlayerController(const ASoftDesignTrainingPlayerController&); \
public:


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASoftDesignTrainingPlayerController(ASoftDesignTrainingPlayerController&&); \
	NO_API ASoftDesignTrainingPlayerController(const ASoftDesignTrainingPlayerController&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASoftDesignTrainingPlayerController); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoftDesignTrainingPlayerController); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASoftDesignTrainingPlayerController)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_6_PROLOG
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_RPC_WRAPPERS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_INCLASS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_INCLASS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h_9_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOFTDESIGNTRAINING_API UClass* StaticClass<class ASoftDesignTrainingPlayerController>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingPlayerController_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
