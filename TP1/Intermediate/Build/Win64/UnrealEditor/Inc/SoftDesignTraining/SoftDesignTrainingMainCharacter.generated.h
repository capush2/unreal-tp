// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOFTDESIGNTRAINING_SoftDesignTrainingMainCharacter_generated_h
#error "SoftDesignTrainingMainCharacter.generated.h already included, missing '#pragma once' in SoftDesignTrainingMainCharacter.h"
#endif
#define SOFTDESIGNTRAINING_SoftDesignTrainingMainCharacter_generated_h

#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_SPARSE_DATA
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_RPC_WRAPPERS
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASoftDesignTrainingMainCharacter(); \
	friend struct Z_Construct_UClass_ASoftDesignTrainingMainCharacter_Statics; \
public: \
	DECLARE_CLASS(ASoftDesignTrainingMainCharacter, ASoftDesignTrainingCharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), NO_API) \
	DECLARE_SERIALIZER(ASoftDesignTrainingMainCharacter)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASoftDesignTrainingMainCharacter(); \
	friend struct Z_Construct_UClass_ASoftDesignTrainingMainCharacter_Statics; \
public: \
	DECLARE_CLASS(ASoftDesignTrainingMainCharacter, ASoftDesignTrainingCharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), NO_API) \
	DECLARE_SERIALIZER(ASoftDesignTrainingMainCharacter)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASoftDesignTrainingMainCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASoftDesignTrainingMainCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASoftDesignTrainingMainCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoftDesignTrainingMainCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASoftDesignTrainingMainCharacter(ASoftDesignTrainingMainCharacter&&); \
	NO_API ASoftDesignTrainingMainCharacter(const ASoftDesignTrainingMainCharacter&); \
public:


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASoftDesignTrainingMainCharacter(ASoftDesignTrainingMainCharacter&&); \
	NO_API ASoftDesignTrainingMainCharacter(const ASoftDesignTrainingMainCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASoftDesignTrainingMainCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoftDesignTrainingMainCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASoftDesignTrainingMainCharacter)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_12_PROLOG
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_RPC_WRAPPERS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_INCLASS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_INCLASS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOFTDESIGNTRAINING_API UClass* StaticClass<class ASoftDesignTrainingMainCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingMainCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
