// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef SOFTDESIGNTRAINING_SoftDesignTrainingCharacter_generated_h
#error "SoftDesignTrainingCharacter.generated.h already included, missing '#pragma once' in SoftDesignTrainingCharacter.h"
#endif
#define SOFTDESIGNTRAINING_SoftDesignTrainingCharacter_generated_h

#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_SPARSE_DATA
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnBeginOverlap);


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnBeginOverlap);


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASoftDesignTrainingCharacter(); \
	friend struct Z_Construct_UClass_ASoftDesignTrainingCharacter_Statics; \
public: \
	DECLARE_CLASS(ASoftDesignTrainingCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), NO_API) \
	DECLARE_SERIALIZER(ASoftDesignTrainingCharacter)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_INCLASS \
private: \
	static void StaticRegisterNativesASoftDesignTrainingCharacter(); \
	friend struct Z_Construct_UClass_ASoftDesignTrainingCharacter_Statics; \
public: \
	DECLARE_CLASS(ASoftDesignTrainingCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), NO_API) \
	DECLARE_SERIALIZER(ASoftDesignTrainingCharacter)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASoftDesignTrainingCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASoftDesignTrainingCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASoftDesignTrainingCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoftDesignTrainingCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASoftDesignTrainingCharacter(ASoftDesignTrainingCharacter&&); \
	NO_API ASoftDesignTrainingCharacter(const ASoftDesignTrainingCharacter&); \
public:


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASoftDesignTrainingCharacter(ASoftDesignTrainingCharacter&&); \
	NO_API ASoftDesignTrainingCharacter(const ASoftDesignTrainingCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASoftDesignTrainingCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoftDesignTrainingCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASoftDesignTrainingCharacter)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_7_PROLOG
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_RPC_WRAPPERS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_INCLASS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_INCLASS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOFTDESIGNTRAINING_API UClass* StaticClass<class ASoftDesignTrainingCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
