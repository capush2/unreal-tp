// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOFTDESIGNTRAINING_SoftDesignTrainingGameMode_generated_h
#error "SoftDesignTrainingGameMode.generated.h already included, missing '#pragma once' in SoftDesignTrainingGameMode.h"
#endif
#define SOFTDESIGNTRAINING_SoftDesignTrainingGameMode_generated_h

#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_SPARSE_DATA
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_RPC_WRAPPERS
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASoftDesignTrainingGameMode(); \
	friend struct Z_Construct_UClass_ASoftDesignTrainingGameMode_Statics; \
public: \
	DECLARE_CLASS(ASoftDesignTrainingGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), SOFTDESIGNTRAINING_API) \
	DECLARE_SERIALIZER(ASoftDesignTrainingGameMode)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_INCLASS \
private: \
	static void StaticRegisterNativesASoftDesignTrainingGameMode(); \
	friend struct Z_Construct_UClass_ASoftDesignTrainingGameMode_Statics; \
public: \
	DECLARE_CLASS(ASoftDesignTrainingGameMode, AGameMode, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), SOFTDESIGNTRAINING_API) \
	DECLARE_SERIALIZER(ASoftDesignTrainingGameMode)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SOFTDESIGNTRAINING_API ASoftDesignTrainingGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASoftDesignTrainingGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SOFTDESIGNTRAINING_API, ASoftDesignTrainingGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoftDesignTrainingGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SOFTDESIGNTRAINING_API ASoftDesignTrainingGameMode(ASoftDesignTrainingGameMode&&); \
	SOFTDESIGNTRAINING_API ASoftDesignTrainingGameMode(const ASoftDesignTrainingGameMode&); \
public:


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SOFTDESIGNTRAINING_API ASoftDesignTrainingGameMode(ASoftDesignTrainingGameMode&&); \
	SOFTDESIGNTRAINING_API ASoftDesignTrainingGameMode(const ASoftDesignTrainingGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SOFTDESIGNTRAINING_API, ASoftDesignTrainingGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASoftDesignTrainingGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASoftDesignTrainingGameMode)


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_6_PROLOG
#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_RPC_WRAPPERS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_INCLASS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_INCLASS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h_9_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOFTDESIGNTRAINING_API UClass* StaticClass<class ASoftDesignTrainingGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_TP1_Source_SoftDesignTraining_SoftDesignTrainingGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
