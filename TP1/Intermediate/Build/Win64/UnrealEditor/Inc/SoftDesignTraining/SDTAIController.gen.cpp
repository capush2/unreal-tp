// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SoftDesignTraining/SDTAIController.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSDTAIController() {}
// Cross Module References
	SOFTDESIGNTRAINING_API UClass* Z_Construct_UClass_ASDTAIController_NoRegister();
	SOFTDESIGNTRAINING_API UClass* Z_Construct_UClass_ASDTAIController();
	AIMODULE_API UClass* Z_Construct_UClass_AAIController();
	UPackage* Z_Construct_UPackage__Script_SoftDesignTraining();
// End Cross Module References
	void ASDTAIController::StaticRegisterNativesASDTAIController()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(ASDTAIController);
	UClass* Z_Construct_UClass_ASDTAIController_NoRegister()
	{
		return ASDTAIController::StaticClass();
	}
	struct Z_Construct_UClass_ASDTAIController_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_maxSpeed_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_maxSpeed;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_maxAccel_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_maxAccel;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_rayDist_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_rayDist;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_rotateSpeed_MetaData[];
#endif
		static const UECodeGen_Private::FFloatPropertyParams NewProp_rotateSpeed;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASDTAIController_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AAIController,
		(UObject* (*)())Z_Construct_UPackage__Script_SoftDesignTraining,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASDTAIController_Statics::Class_MetaDataParams[] = {
		{ "ClassGroupNames", "AI" },
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Collision Rendering Transformation" },
		{ "IncludePath", "SDTAIController.h" },
		{ "ModuleRelativePath", "SDTAIController.h" },
	};
#endif
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxSpeed_MetaData[] = {
		{ "Category", "SDTAIController" },
		{ "ModuleRelativePath", "SDTAIController.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxSpeed = { "maxSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASDTAIController, maxSpeed), METADATA_PARAMS(Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxSpeed_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxAccel_MetaData[] = {
		{ "Category", "SDTAIController" },
		{ "ModuleRelativePath", "SDTAIController.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxAccel = { "maxAccel", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASDTAIController, maxAccel), METADATA_PARAMS(Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxAccel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxAccel_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASDTAIController_Statics::NewProp_rayDist_MetaData[] = {
		{ "Category", "SDTAIController" },
		{ "ModuleRelativePath", "SDTAIController.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASDTAIController_Statics::NewProp_rayDist = { "rayDist", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASDTAIController, rayDist), METADATA_PARAMS(Z_Construct_UClass_ASDTAIController_Statics::NewProp_rayDist_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASDTAIController_Statics::NewProp_rayDist_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASDTAIController_Statics::NewProp_rotateSpeed_MetaData[] = {
		{ "Category", "SDTAIController" },
		{ "ModuleRelativePath", "SDTAIController.h" },
	};
#endif
	const UECodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ASDTAIController_Statics::NewProp_rotateSpeed = { "rotateSpeed", nullptr, (EPropertyFlags)0x0010000000000001, UECodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ASDTAIController, rotateSpeed), METADATA_PARAMS(Z_Construct_UClass_ASDTAIController_Statics::NewProp_rotateSpeed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ASDTAIController_Statics::NewProp_rotateSpeed_MetaData)) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ASDTAIController_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxSpeed,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASDTAIController_Statics::NewProp_maxAccel,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASDTAIController_Statics::NewProp_rayDist,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ASDTAIController_Statics::NewProp_rotateSpeed,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASDTAIController_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASDTAIController>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_ASDTAIController_Statics::ClassParams = {
		&ASDTAIController::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ASDTAIController_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ASDTAIController_Statics::PropPointers),
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASDTAIController_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASDTAIController_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASDTAIController()
	{
		if (!Z_Registration_Info_UClass_ASDTAIController.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_ASDTAIController.OuterSingleton, Z_Construct_UClass_ASDTAIController_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_ASDTAIController.OuterSingleton;
	}
	template<> SOFTDESIGNTRAINING_API UClass* StaticClass<ASDTAIController>()
	{
		return ASDTAIController::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASDTAIController);
	struct Z_CompiledInDeferFile_FID_TP1_Source_SoftDesignTraining_SDTAIController_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_TP1_Source_SoftDesignTraining_SDTAIController_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_ASDTAIController, ASDTAIController::StaticClass, TEXT("ASDTAIController"), &Z_Registration_Info_UClass_ASDTAIController, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(ASDTAIController), 3427902971U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_TP1_Source_SoftDesignTraining_SDTAIController_h_3429001178(TEXT("/Script/SoftDesignTraining"),
		Z_CompiledInDeferFile_FID_TP1_Source_SoftDesignTraining_SDTAIController_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_TP1_Source_SoftDesignTraining_SDTAIController_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
