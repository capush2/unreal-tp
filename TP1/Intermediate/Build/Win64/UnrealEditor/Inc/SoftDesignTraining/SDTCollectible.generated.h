// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOFTDESIGNTRAINING_SDTCollectible_generated_h
#error "SDTCollectible.generated.h already included, missing '#pragma once' in SDTCollectible.h"
#endif
#define SOFTDESIGNTRAINING_SDTCollectible_generated_h

#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_SPARSE_DATA
#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_RPC_WRAPPERS
#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASDTCollectible(); \
	friend struct Z_Construct_UClass_ASDTCollectible_Statics; \
public: \
	DECLARE_CLASS(ASDTCollectible, AStaticMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), NO_API) \
	DECLARE_SERIALIZER(ASDTCollectible)


#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASDTCollectible(); \
	friend struct Z_Construct_UClass_ASDTCollectible_Statics; \
public: \
	DECLARE_CLASS(ASDTCollectible, AStaticMeshActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SoftDesignTraining"), NO_API) \
	DECLARE_SERIALIZER(ASDTCollectible)


#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASDTCollectible(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASDTCollectible) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASDTCollectible); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASDTCollectible); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASDTCollectible(ASDTCollectible&&); \
	NO_API ASDTCollectible(const ASDTCollectible&); \
public:


#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASDTCollectible(ASDTCollectible&&); \
	NO_API ASDTCollectible(const ASDTCollectible&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASDTCollectible); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASDTCollectible); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ASDTCollectible)


#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_12_PROLOG
#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_RPC_WRAPPERS \
	FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_INCLASS \
	FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_SPARSE_DATA \
	FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_INCLASS_NO_PURE_DECLS \
	FID_TP1_Source_SoftDesignTraining_SDTCollectible_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SOFTDESIGNTRAINING_API UClass* StaticClass<class ASDTCollectible>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_TP1_Source_SoftDesignTraining_SDTCollectible_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
