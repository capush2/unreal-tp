// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSoftDesignTraining_init() {}
	static FPackageRegistrationInfo Z_Registration_Info_UPackage__Script_SoftDesignTraining;
	FORCENOINLINE UPackage* Z_Construct_UPackage__Script_SoftDesignTraining()
	{
		if (!Z_Registration_Info_UPackage__Script_SoftDesignTraining.OuterSingleton)
		{
			static const UECodeGen_Private::FPackageParams PackageParams = {
				"/Script/SoftDesignTraining",
				nullptr,
				0,
				PKG_CompiledIn | 0x00000000,
				0x84C653BF,
				0x83A1D8AD,
				METADATA_PARAMS(nullptr, 0)
			};
			UECodeGen_Private::ConstructUPackage(Z_Registration_Info_UPackage__Script_SoftDesignTraining.OuterSingleton, PackageParams);
		}
		return Z_Registration_Info_UPackage__Script_SoftDesignTraining.OuterSingleton;
	}
	static FRegisterCompiledInInfo Z_CompiledInDeferPackage_UPackage__Script_SoftDesignTraining(Z_Construct_UPackage__Script_SoftDesignTraining, TEXT("/Script/SoftDesignTraining"), Z_Registration_Info_UPackage__Script_SoftDesignTraining, CONSTRUCT_RELOAD_VERSION_INFO(FPackageReloadVersionInfo, 0x84C653BF, 0x83A1D8AD));
PRAGMA_ENABLE_DEPRECATION_WARNINGS
