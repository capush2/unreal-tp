// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"

#include "SDTAIController.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = AI, config = Game)
class SOFTDESIGNTRAINING_API ASDTAIController : public AAIController
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere)
        float maxSpeed = 500.0f;

    UPROPERTY(EditAnywhere)
        float maxAccel = 250.0f;

    UPROPERTY(EditAnywhere)
        float rayDist = 400.0f;

    UPROPERTY(EditAnywhere)
        float rotateSpeed = 1.0f;

    virtual void BeginPlay() override;
    virtual void Tick(float deltaTime) override;
};
