// Fill out your copyright notice in the Description page of Project Settings.

#include "SDTAIController.h"
#include "SoftDesignTraining.h"
#include <Runtime/Engine/Classes/Kismet/KismetMathLibrary.h>

void ASDTAIController::BeginPlay() {
	AAIController::BeginPlay();
	UCharacterMovementComponent* movement = GetCharacter()->GetCharacterMovement();
	movement->MaxAcceleration = maxAccel;
	movement->MaxWalkSpeed = maxSpeed;
}

void ASDTAIController::Tick(float deltaTime)
{
	APawn* pawn = GetPawn();
	FVector pos = pawn->GetActorLocation();
	FVector forward = pawn->GetActorForwardVector();
	pawn->AddMovementInput(forward);

	FHitResult out;
	bool isHit = GetWorld()->LineTraceSingleByObjectType(out, pos,
		pos + forward * rayDist, FCollisionObjectQueryParams::AllStaticObjects);
	if (isHit) {
		DrawDebugLine(GetWorld(), out.ImpactPoint, out.ImpactPoint + out.ImpactNormal * rayDist, FColor::Green);
		FRotator rotator = UKismetMathLibrary::FindLookAtRotation(forward, -1 * out.ImpactNormal);
		int turnRight = rotator.Euler().Z >= 0 ? -1 : 1;
		pawn->AddActorWorldRotation(FQuat::MakeFromEuler(FVector(0, 0, rotateSpeed * turnRight)));
		GEngine->AddOnScreenDebugMessage(-1, 0.1f, FColor::Blue, FString::SanitizeFloat(rotator.Euler().Z));
		
	}

	DrawDebugLine(GetWorld(), pos, pos + forward * rayDist, FColor::Red);
}




